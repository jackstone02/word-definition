<?php

namespace App\Http\Controllers;

use App\PreviousSearch;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DataTables;

class SearchController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
		$results = [];
		return view('search',['results' => $results]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
		PreviousSearch::create([
			'name' => $request->input('name')
		]);

		$results = $this->definitions($request->input('name'));

		return view('search', [
			'results' => json_decode($results, true)
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\PreviousSearch  $previousSearch
	 * @return \Illuminate\Http\Response
	 */
	public function show(PreviousSearch $previousSearch)
	{
		//
		return view('previous_searches', [
			'results' => $previousSearch->orderBy('created_at','desc')->get()
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\PreviousSearch  $previousSearch
	 * @return \Illuminate\Http\Response
	 */
	public function edit(PreviousSearch $previousSearch)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\PreviousSearch  $previousSearch
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, PreviousSearch $previousSearch)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\PreviousSearch  $previousSearch
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(PreviousSearch $previousSearch)
	{
		//
	}

	public function definitions($word)
	{

		$client = new \GuzzleHttp\Client(['verify' => false]);
		// $res = $client->request('GET', 'https://wordsapiv1.p.mashape.com/words/'.$word.'/definitions', [
		//     'headers' => [
		//         "x-rapidapi-host" => "wordsapiv1.p.rapidapi.com",
		//         "x-rapidapi-key" => "a25eff774amshddcccbe792fa613p1ebcd6jsn2f37fa2d8555"
		//      ]
		// ]);

		try {
			$res = $client->request('GET', 'https://wordsapiv1.p.mashape.com/words/'.$word.'/definitions', [
				'headers' => [
					"x-rapidapi-host" => "wordsapiv1.p.rapidapi.com",
					"x-rapidapi-key" => "a25eff774amshddcccbe792fa613p1ebcd6jsn2f37fa2d8555"
				 ]
			]);
		}
		catch (\GuzzleHttp\Exception\ClientException $e) {
			$res = $e->getResponse();
			$responseBodyAsString = $res->getBody()->getContents();
		}

		return $res->getBody();
	}

	public function index2(Request $request)
	{
		if ($request->ajax()) {
			$data = PreviousSearch::latest()->get();
			return DataTables::of($data)
					->addIndexColumn()
					->addColumn('action', function($row){
						   $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';
							return $btn;
					})
					->rawColumns(['action'])
					->make(true);
		}
	  
		return view('report');
	}


}
