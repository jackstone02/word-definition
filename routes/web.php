<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/search', 'SearchController@index')->name('search');
Route::post('/search', 'SearchController@store');
Route::get('/previous_searches', 'SearchController@show')->name('previous_searches');

Route::get('/report', 'SearchController@index2')->name('report.index2');
// Route::get('report', ['uses'=>'SearchController@index2', 'as'=>'report.index2']);