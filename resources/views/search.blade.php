@extends ('layout')

@section ('content')
	<form method="POST" action="/search">
		{{ csrf_field() }}
	  	<div class="form-group">
			<input type="text" class="form-control" id="name" name="name" required>
	  	</div>

	  	<div class="form-group">
	  		<button type="submit" class="btn btn-primary">Search</button>
	  	</div>

	</form>

	@if( $results )
  	<div class="form-group">
  		@if( isset($results["word"]) )
	  		<p><b>{{ $results["word"] }}</b></p>
	  		<p>Definition/s:</p>
	  		<ol>
	  			@foreach( $results["definitions"] as $k => $v )
	  				<li><i>({{ $results["definitions"][$k]["partOfSpeech"] }})</i> {{ $results["definitions"][$k]["definition"] }}</li>
	  			@endforeach
	  		</ol>
  		@else
  			<p><i>Word not found</i></p>
  		@endif
  	</div>
  	@endif

@endsection