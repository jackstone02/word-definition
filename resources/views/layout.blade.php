<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Laravel Exam</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		@yield('styles')

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		@yield('scripts')
	</head>

	<body>

		<div class="container">

			<div class="row">

				<div class="col-sm-12 blog-main">
					<br>
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th class="{{ Request::path() === 'search' ? 'active' : ''}}"><a href="{{ url('/search') }}">Search</a></th>
								<th class="{{ Request::path() === 'previous_searches' ? 'active' : ''}}"><a href="{{ url('/previous_searches') }}">Previous Searches</a></th>
								<th class="{{ Request::path() === 'report' ? 'active' : ''}}"><a href="{{ url('/report') }}">Report</a></th>
							</tr>
						</thead>
						<tr>
							<td colspan="3">

								@yield ('content')

							</td>
						</tr>
					</table>

				</div>

			</div><!-- /.row -->

		</div><!-- /.container -->

	</body>
</html>