@extends ('layout')

@section ('content')
	<table class="table table-striped table-bordered">
		@foreach( $results as $item )
		<tr>
			<td>{{ $item->name }}</td>
		</tr>
		@endforeach
	</table>
@endsection